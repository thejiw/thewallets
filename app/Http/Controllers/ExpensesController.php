<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ! Expenses
        return Expenses::latest()->get();
    }

    public function count(){
        return Expenses::latest()->get();
    }


    public function store(Request $request)
    {
        //
        $expense = new Expenses();
        $expense->name = $request->name;
        $expense->total_money = $request->total_money;
        $expense->detail = $request->detail;
        $expense->save();
        return $expense;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $expense = Expenses::findOrFail($id);
        $expense->completed = $request->compreted;
        $expense->update();
        return $expense;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $expense = Expenses::findOrFail($id);
        $expense->delete();
        return 204;
    }
}
