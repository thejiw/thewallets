<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Income;

class IncomeController extends Controller
{
   
    public function index()
    {
        return Income::latest()->get();
    }

    public function count(){
        return Income::latest()->get();
    }


    public function store(Request $request)
    {
        $income = new Income();
        $income->name = $request->name;
        $income->total_money = $request->total_money;
        $income->detail = $request->detail;
        $income->save();
        return $income;
    }



    public function update(Request $request, $id)
    {
        $income = Income::findOrFail($id);
        $income->completed = $request->compreted;
        $income->update();
        return $income;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $income = Income::findOrFail($id);
        $income->delete();
        return 204;
    }
}
