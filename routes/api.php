<?php



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// route api
Route::resource('income','IncomeController', [
    'except' => ['create','edit','show'],
]);
Route::resource('income/count','IncomeController', [
    'except' => ['count'],
]);


Route::resource('expense','ExpensesController', [
    'except' => ['create','edit','show'],
]);
Route::resource('expense/count','ExpensesController', [
    'except' => ['count'],
]);




// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
