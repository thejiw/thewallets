<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('income');
        Schema::create('income', function (Blueprint $table) {
            $table->increments('income_id');
            $table->integer('role_id')->unsigned();
            $table->string('name');
            $table->string('email',50)->unique();
            $table->integer('total_money');
            $table->string('detail');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('income');
    }
}
